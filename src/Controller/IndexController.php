<?php

namespace App\Controller;

use App\Service\CallbackService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends AbstractController
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/curl")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param CallbackService $service
     * @return Response
     */
    public function createCallbackRequest(Request $request, CallbackService $service): Response
    {
        $response = $service->create($request);

        return $this->json(['status' => 'ok', 'curl' => $response]);
    }
}