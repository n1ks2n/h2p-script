<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class CallbackService
{
    private const ALGO = 'md5';

    private const BASE_COMMAND = 'curl -X "POST" http://proxy.billing.int.iqoption.com/__broadcaster/help2pay \
    -d ';

    public function create(Request $request): string
    {
        $data = $request->request->all();
        $key = $this->calcKey($data);

        return $this->createCurlCommand($data, $key);
    }

    private function calcKey(array $data): string
    {
        $hashedText = $data['MerchantCode'] . $data ['TransactionID'] . $data['MemberCode'] . $data['Amount'] .
            $data['CurrencyCode'] . $data['Status'] . $data['SecurityCode'];
        return hash(self::ALGO, $hashedText);
    }

    private function createCurlCommand(array $data, string $hashedValue)
    {
        $dataString = '"';

        foreach ($data as $key => $value) {
            $dataString .= $key . '=' . $value . '&';
        }

        $dataString .= 'Key=' . $hashedValue . '"';

        return self:: BASE_COMMAND . $dataString;
    }
}